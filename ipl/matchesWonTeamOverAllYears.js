function matchesWonTeamOverAllYears(matches){
    const result={};
    for(let match of matches){
        const year= match.season;
        result[year]={};
    }
    for(let team of matches){
         let  t=team.winner;
         let year=team.season;
         if(t=='')
         t="noResult";
         if(result[year][t]!=undefined)
         result[year][t]+=1;
         else
         result[year][t]=1;
    }//console.log(result);
    return result;
}

module.exports = matchesWonTeamOverAllYears;
