function positionOfTeams(matches){
    const result={};
    const result1={};
    const se=new Set();
    for(let match of matches){
        const year= match.season;
        let t=match.winner;
        se.add(t);
        result[year]={};
    }
    for(let team of matches){
         let  t=team.winner;
         let year=team.season;
         if(result[year][t]!=undefined)
         result[year][t]+=1;
         else
         result[year][t]=1;
    }
    let se1=[...se];
    for(let year in result){
        let k=getKey(se1,result[year]);
        for(let i of k){
            result[year][i]=0;
          }
    }
    function getKey(obj1,obj2){
        return obj1.filter(function(x){
          return !(x in obj2);   
        });
    }
    let newArr={};
    for(let year in result){
        for(let team in result[year]){
            if(newArr[year])
         newArr[year].push(result[year][team]);
         else{
         newArr[year]=[];
         newArr[year].push(result[year][team])};
        }
    }
    for(let year in newArr){
        newArr[year]=[...new Set(newArr[year].sort((a,b)=>b-a))];
    }
   l1: for(let year in result){
        for(let team in result[year]){
            result[year][team]=newArr[year].indexOf(result[year][team]) +1;
        }
    }
    
   for(let year in result){
     for(let team in result[year]){
         if(result1[team])
         result1[team].push(result[year][team]);
         else{
             if(result1[team]==undefined){
             result1[team]=[];
             result1[team].push(result[year][team]);}
             else
             result1[team].push(0);
         }
     }
    }
    delete result1["noResult"];
    return result1;
}
module.exports = positionOfTeams;