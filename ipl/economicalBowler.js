function economicalBowler(matches,deliveries){
    const result={};
    const result1 =[];
    const k=[];
    for(let team of matches){
        if(team.season=='2015')
        k.push(team.id);
    }
    for(let team of deliveries){
        if(k.includes(team.match_id)){
            let bowler= team.bowler;
            let run = team.total_runs;
            let ball = team.ball;
            if(result[bowler]!==null){
                result[bowler]+= parseInt(run);
                if(ball==6)
                result1[bowler]+=1; 
                }
                else{
                result[bowler]=parseInt(run);
                result1[bowler]=0;
        }}
        }//console.log(result);
        let newArr=[];
              for(let team in result){
               result[team]=Number((result[team]/result1[team]).toFixed(2));
               newArr.push({"bowler":team,"economy":result[team]});
              }
              newArr=newArr.sort((a,b)=>a["economy"]-b["economy"]).filter((x,i)=>i<10);
              //console.log(newArr);
                return newArr;
            }
module.exports = economicalBowler;