const csv = require("csvtojson");
const fs = require("fs");
const MATCHES_FILE_PATH = "./csv_data/matches.csv";
const DELIVERIES_FILE_PATH = "./csv_data/deliveries.csv";
const JSON_OUTPUT_FILE_PATH = "./public/data.json";
const matchesPlayedPerYear = require("./ipl/matchesPlayedPerYear");
const matchesWonTeamOverAllYears = require("./ipl/matchesWonTeamOverAllYears");
const extraRuns = require("./ipl/extraRuns");
const economicalBowler = require("./ipl/economicalBowler");
const positionOfTeams = require("./ipl/positionOfTeams");
function main() {
    csv()
      .fromFile(MATCHES_FILE_PATH)
      .then(matches => {
        csv()
        .fromFile(DELIVERIES_FILE_PATH)
        .then(deliveries => {
         let result2 = extraRuns(matches,deliveries);
         let result3 = economicalBowler(matches,deliveries);
         let [result,result1,result4] = [matchesPlayedPerYear(matches),matchesWonTeamOverAllYears(matches),positionOfTeams(matches)];
         saveMatchesPlayedPerYear(result,result1,result2,result3,result4);
        });
       
      });
  }
  function saveMatchesPlayedPerYear(result,result1,result2,result3,result4) {
    const jsonData = {
        matchesPlayedPerYear :result,
        matchesWonTeamOverAllYears: result1,
        extraRuns: result2,
        economicalBowler :result3,
        positionOfTeams :result4
    };
    const jsonString = JSON.stringify(jsonData);
    fs.writeFile(JSON_OUTPUT_FILE_PATH, jsonString, "utf8", err => {
      if (err) {
        console.error(err);
      }
    });
  }
main();