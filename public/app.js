function fetchAndVisualizeData() {
    fetch("./data.json")
      .then(r => r.json())
      .then(visualizeData);
  }
  
  fetchAndVisualizeData();
  
  function visualizeData(data) {
    visualizeMatchesPlayedPerYear(data.matchesPlayedPerYear,data.matchesWonTeamOverAllYears,data.extraRuns,data.economicalBowler,data.positionOfTeams);
    return;
  }
  
  function visualizeMatchesPlayedPerYear(matchesPlayedPerYear,matchesWonTeamOverAllYears,extraRuns,economicalBowler,positionOfTeams) {
    const seriesData1 = [];
    for (let year in matchesPlayedPerYear) {
      seriesData1.push([year, matchesPlayedPerYear[year]]);
    }
    Highcharts.chart("matches-played-per-year", {
      chart: {
        type: "column"
      },
      title: {
        text: "Matches Played Per Year"
      },
      subtitle: {
        text:
          'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
      },
      xAxis: {
        type: "category"
      },
      yAxis: {
        min: 0,
        title: {
          text: "Matches"
        }
      },
      series: [
        {
          name: "Years",
          data: seriesData1
        }
      ]
    });
    const seriesData=[];
    let y=[];
    for(let year in matchesWonTeamOverAllYears){
        y.push(year);
      for(let team in matchesWonTeamOverAllYears[year])
      if(seriesData[team])
      seriesData[team].push(matchesWonTeamOverAllYears[year][team]);
      else{
      seriesData[team]=[];
      seriesData[team].push(matchesWonTeamOverAllYears[year][team]);}
      }
      
  Highcharts.chart('matches-played-all-year', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Matches Won By Each Team Over All Years'
    },
    subtitle: {
        text: 'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
    },
    xAxis: {
        categories:(function() {
           return y;
        }()),
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Matches Won'
        }
    },
    
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series :  (function() {
        var series = [];
        for (var i in seriesData) {
            series.push({
                name:i,
                data:seriesData[i]
            });
        }
        return series;
    }())
});
const seriesData2 = [];
    for (let team in extraRuns) {
      seriesData2.push([team, extraRuns[team]]);
    }
  
    Highcharts.chart("extra-runs", {
      chart: {
        type: "column"
      },
      title: {
        text: "Extra Runs By Each Team in 2016"
      },
      subtitle: {
        text:
          'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
      },
      xAxis: {
        type: "category"
      },
      yAxis: {
        min: 0,
        title: {
          text: "Runs"
        }
      },
      series: [
        {
          name: "Teams",
          data: seriesData2
        }
      ]
    });
    const seriesData3 = [];
    for (let team of economicalBowler) {
      seriesData3.push([team["bowler"], team["economy"]]);
    }

    Highcharts.chart("economical-bowler", {
      chart: {
        type: "column"
      },
      title: {
        text: "Economic Rates of Top 10 Bowlers of 2015"
      },
      subtitle: {
        text:
          'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
      },
      xAxis: {
        type: "category"
      },
      yAxis: {
        min: 0,
        title: {
          text: "Economy Rate"
        }
      },
      series: [
        {
          name: "Bowler",
          data: seriesData3
        }
      ]
    });

    const seriesData4=[];
    for(let team in positionOfTeams){
      seriesData4.push([team,positionOfTeams[team]]);
      }

    Highcharts.chart('position-of-teams', {

      title: {
          text: 'Position of Teams'
      },
  
      subtitle: {
          text:'Source: <a href="https://www.kaggle.com/nowke9/ipldata/data">IPL Dataset</a>'
      },
  
      yAxis: {
        reversed :true,
        showFirstLabel: false,
        showLastLabel: true,
          title: {
              text: 'Position'
          }
      },
  
      xAxis: {
          accessibility: {
              rangeDescription: 'Range: 2008 to 2019'
          }
      },
  
      legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle'
      },
  
      plotOptions: {
          series: {
              label: {
                  connectorAllowed: false
              },
              pointStart: 2008
          }
      },
  
      series: (function(){
        var series=[];
        for(let i in seriesData4){
          series.push({
            name: seriesData4[i][0],
            data :seriesData4[i][1]
          });
        }
           return series;
      }()),

      responsive: {
          rules: [{
              condition: {
                  maxWidth: 500
              },
              chartOptions: {
                  legend: {
                      layout: 'horizontal',
                      align: 'center',
                      verticalAlign: 'bottom'
                  }
              }
          }]
      }
  
  });
  }